package com.newstok.model;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@DynamoDBTable(tableName = "credibility")
public class CredibilityDao {
	
	@DynamoDBHashKey(attributeName = "user_id")
	String userId;
	
	@DynamoDBAttribute(attributeName = "credibility")
	String credibility;
}
