package com.newstok.model;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBRangeKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@DynamoDBTable(tableName = "feedbacks")
public class FeedbackDao {
	
	@DynamoDBHashKey(attributeName = "news_id")
	String newsId;
	
	@DynamoDBAttribute(attributeName = "reported_id")
	String reportedId;
	
	@DynamoDBAttribute(attributeName = "user_id")
	String userId;
	
	@DynamoDBAttribute(attributeName = "feedback")
	String feedback;
	
	@DynamoDBRangeKey(attributeName = "timestamp")
	String timestamp;
}
