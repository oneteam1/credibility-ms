package com.newstok.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Feedback {
	String newsId;
	String reportedId;
	String userId;
	String feedback;
	String timestamp;
}
