package com.newstok.service;

import java.util.*;
import java.util.function.Consumer;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBQueryExpression;
import com.amazonaws.services.dynamodbv2.datamodeling.PaginatedQueryList;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.newstok.model.CredibilityDao;
import com.newstok.model.Feedback;
import com.newstok.model.FeedbackDao;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
@AllArgsConstructor
public class MessageListenerService {

	final private DynamoDBMapper mapper;

	@Bean
	public Consumer<Feedback> consume() {
		return feedback -> {
			log.info("INCOMING MESSAGE : " + feedback.toString());

			this.fetchExistingDetailsFromDatabase(feedback);
		};
	}
	
	public void fetchExistingDetailsFromDatabase(Feedback message) {
		log.info("fetchExistingDetailsFromDatabase start");
		
		String reporterId = message.getReportedId();
		
		DynamoDBQueryExpression<FeedbackDao> queryExpression = 
	    		new DynamoDBQueryExpression<FeedbackDao>().withIndexName("reported_id-index");

	    Map<String, AttributeValue> valueMap = new HashMap<>();

	    valueMap.put(":reporterId", new AttributeValue(reporterId));

	    queryExpression.withKeyConditionExpression("reported_id = :reporterId")
	    	.withExpressionAttributeValues(valueMap).withConsistentRead(false);
	    PaginatedQueryList<FeedbackDao> list;
	    try {
	    	list = mapper.query(FeedbackDao.class, queryExpression);
	    	log.info("Fetched list of size : " + list.size());
	    } catch (Exception e) {
	    	log.error("Error fetching FEEDBACKS for reporter: " + reporterId);
	    	e.printStackTrace();
	    	throw new InternalError(e.getMessage());
		}
	    
	    if (!list.isEmpty()) {
	    	calculateCredibilityAlgorithm(list, reporterId);
	    }
	    
	    log.info("fetchExistingDetailsFromDatabase end");
	}
	
	public void calculateCredibilityAlgorithm(PaginatedQueryList<FeedbackDao> list, String reporterId) {
		
		log.info("calculateCredibilityAlgorithm START");
		
		OptionalDouble credibility = list.stream().mapToInt(i->Integer.parseInt(i.getFeedback())).average();

		if (credibility.isPresent()) {
			log.info("Calculated credibility == " + credibility.getAsDouble());
			persistValue(credibility, reporterId);
		} else {
			log.info("No credibility calculated");
		}

		log.info("calculateCredibilityAlgorithm END");
	}

	private void persistValue(OptionalDouble credibility, String reporterId) {
		
		log.info("persistValue START");
		
		CredibilityDao credibilityObj = CredibilityDao.builder()
				.credibility(String.valueOf(credibility.getAsDouble()))
				.userId(reporterId)
				.build();
	    try {
	    	mapper.save(credibilityObj);
	    } catch (Exception e) {
	    	log.error("Error saving CREDIBILITY : " + credibilityObj.toString());
	    	e.printStackTrace();
	    	throw new InternalError(e.getMessage());
		}
	    
	    log.info("persistValue END");
	}
}
